package events

import (
	"log"

	"../store"
)

func (r *Event) Route() interface{} {
	switch r.Type {
	case "tellEvent":
		log.Printf("Got tell Event")
		tellEvent := NewTell(r)
		store.TellSave(tellEvent)
		return tellEvent
	case "msdpEvent":
		return r.routeMsdp()
	default:
		log.Printf("No route for event type %s", r.Type)
		return r
	}
}
