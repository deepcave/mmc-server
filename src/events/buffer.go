package events

type EventBufferItem struct {
	Time uint32
	Type uint32
	Item *interface{}
}

var buffer []EventBufferItem

func BufferInit() {
	buffer = make([]EventBufferItem, 2048, 10240)
}
