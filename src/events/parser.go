package events

import (
	"log"
	"strconv"
	"strings"
	"time"
)

type Event struct {
	Time  time.Time
	Type  string
	Value string
}

func Parse(line string) *Event {
	e := Event{}
	splited := strings.SplitN(line, "#", 3)
	if len(splited) < 3 {
		log.Printf("ERROR: event line broken : %s", line)
		return nil
	}

	uTime, err := strconv.Atoi(splited[0])
	if err != nil {
		log.Printf("ERROR: event time broken : [%s]", splited[0])
		return nil
	}
	e.Time = time.Unix(int64(uTime), 0)
	e.Type = splited[1]
	e.Value = splited[2]
	return &e
}
