package events

import (
	"log"
	"strings"

	"../types"
)

var LastTell *types.Tell

func NewTell(e *Event) *types.Tell {
	tell := types.Tell{Type: "tell"}
	splited := strings.SplitN(e.Value, "#", 3)
	if len(splited) < 3 {
		log.Printf("WARN: go a fucked tell : %s", e.Value)
		return nil
	}
	tell.Time = e.Time
	tell.TellType = splited[0]
	tell.From = splited[1]
	tell.Message = splited[2]
	LastTell = &tell
	return &tell
}
