package events

import (
	"time"

	"../store"
	"../utils"
)

type MsdpEvent struct {
	Time     time.Time
	Type     string
	Value    string
	MsdpType string
	Msdp     map[string]interface{}
}

func (e *Event) routeMsdp() MsdpEvent {
	msdpEvent := MsdpEvent{
		Time:  e.Time,
		Type:  e.Type,
		Value: e.Value,
		Msdp:  utils.MsdpDecode([]byte(e.Value))}

	if room, e := msdpEvent.Msdp["ROOM"]; e {
		store.MsdpSaveRoom(room.(map[string]interface{}))
	}
	return msdpEvent
}
