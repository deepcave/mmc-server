package types

import "time"

type Tell struct {
	Time     time.Time
	Type     string
	TellType string
	From     string
	Message  string
}

func (t *Tell) GetUT() int64 {
	return t.Time.Unix()
}

func (t *Tell) SetUT(ts int64) {
	a := time.Unix(ts, 0)
	t.Time = a
}
