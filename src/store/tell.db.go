package store

import (
	"log"

	"../types"
)

func TellSave(tell *types.Tell) {
	InitStms()
	if tell == nil {
		return
	}
	log.Println(tell.GetUT(), tell.TellType, tell.From, tell.Message)
	stmSaveTell.Exec(tell.GetUT(), tell.TellType, tell.From, tell.Message)
}

func TellGets() *[]types.Tell {
	res, err := Db.Query("SELECT Time,Type,Name,Message FROM Messages ORDER BY Time DESC Limit 10240")
	if err != nil {
		return nil
	}
	result := make([]types.Tell, 0)
	for res.Next() {
		tell := types.Tell{}
		var time int64

		res.Scan(&time, &tell.TellType, &tell.From, &tell.Message)
		tell.SetUT(time)
		result = append(result, tell)
	}
	return &result
}
