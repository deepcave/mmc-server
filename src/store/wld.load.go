package store

import (
	"database/sql"
	"log"
)

var wld tWldIndexLst

func initWld() {
	wld.new = createWld()
	wld.old = createWld()
}

func Wld() *tWldIndex {
	return wld.new
}

func LoadOld() {

	var err error
	cnt := 0
	log.Println("start read")
	var rows *sql.Rows

	//Грузим зоны
	if rows, err = oldDb.Query("select * FROM zones"); err != nil {
		log.Println("DB load error", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var Vnum, Name string
		cnt++
		rows.Scan(&Vnum, &Name)
		wld.old.newZone(Vnum, Name)
	}

	//Грузим комнаты
	if rows, err = oldDb.Query("select * FROM rooms"); err != nil {
		log.Println("DB load error", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var Vnum, Zone, Name string
		cnt++
		rows.Scan(&Vnum, &Zone, &Name)
		wld.old.newRoom(Vnum, Name, Zone, "")
	}

	//Грузим выходы
	if rows, err = oldDb.Query("select * FROM exits"); err != nil {
		log.Println("DB load erro", err)
	}
	defer rows.Close()
	for rows.Next() {
		var src, dst, direct string
		rows.Scan(&src, &dst, &direct)
		wld.old.newExit(src, dst, direct)
	}
	log.Println("stop read", wld.old.GetRoom("85532"))
}

func loadMap() {

	if rows, err := Db.Query("SELECT VNUM,NAME FROM Zones"); err == nil {
		var VNUM, NAME string
		defer rows.Close()
		for rows.Next() {
			rows.Scan(&VNUM, &NAME)
			wld.new.newZone(VNUM, NAME)
		}
	} else {
		log.Println("error load zones ", err)
	}

	if rows, err := Db.Query("SELECT VNUM,ZONE,NAME,TERRAIN FROM Rooms"); err == nil {
		var VNUM, ZONE, NAME, TERRAIN string
		defer rows.Close()
		for rows.Next() {
			rows.Scan(&VNUM, &ZONE, &NAME, &TERRAIN)
			wld.new.newRoom(VNUM, NAME, ZONE, TERRAIN)
		}
	} else {
		log.Println("error load rooms ", err)

	}

	if rows, err := Db.Query("SELECT SRC,DST,DIRECT FROM RoomExits"); err == nil {
		var SRC, DST, DIRECT string
		defer rows.Close()
		for rows.Next() {
			rows.Scan(&SRC, &DST, &DIRECT)
			wld.new.newExit(SRC, DST, DIRECT)
			//wld.new.newRoom(VNUM, NAME, ZONE, TERRAIN)
		}
	} else {
		log.Println("error load exits ", err)
	}
}
