package store

import (
	"log"
)

type tRoomExits map[string]string

type tRoom struct {
	wld     *tWldIndex
	ZONE    string
	VNUM    string
	NAME    string
	TERRAIN string
	EXITS   tRoomExits
}

type tZone struct {
	wld   *tWldIndex
	VNUM  string
	NAME  string
	ROOMS []*tRoom
}

type tWldIndexLst struct {
	old *tWldIndex
	new *tWldIndex
}

type tWldIndex struct {
	Rooms map[string]*tRoom
	Zones map[string]*tZone
	exits map[string]map[string]string
}

func createWld() *tWldIndex {
	wld := tWldIndex{
		Rooms: make(map[string]*tRoom),
		Zones: make(map[string]*tZone),
		exits: make(map[string]map[string]string)}
	return &wld
}

func (r *tRoom) setExit(DIRECT string, DST string) {
	if _, e := r.EXITS[DIRECT]; !e {
		r.EXITS[DIRECT] = DST
	}
}

func (r *tRoom) getExitRoom(DIRECT string) *tRoom {
	if VNUM, e := r.EXITS[DIRECT]; !e {
		return nil
	} else {
		return r.wld.GetRoom(VNUM)
	}
}

func (w *tWldIndex) GetRoom(VNUM string) *tRoom {
	if room, e := w.Rooms[VNUM]; !e {
		return nil
	} else {
		return room
	}
}

func (w *tWldIndex) GetZone(VNUM string) *tZone {
	if zone, e := w.Zones[VNUM]; !e {
		return nil
	} else {
		return zone
	}
}

func (z *tZone) pushRoom(room *tRoom) {
	z.ROOMS = append(z.ROOMS, room)
}

func (w *tWldIndex) newRoom(VNUM string, NAME string, ZONE string, TERRAIN string) *tRoom {
	newRoom := tRoom{
		VNUM:    VNUM,
		NAME:    NAME,
		ZONE:    ZONE,
		TERRAIN: TERRAIN,
		EXITS:   make(tRoomExits),
		wld:     w}
	w.Rooms[VNUM] = &newRoom
	zone := w.GetZone(ZONE)
	zone.pushRoom(&newRoom)
	return &newRoom
}

func (w *tWldIndex) newZone(VNUM string, NAME string) *tZone {
	newZone := tZone{
		VNUM:  VNUM,
		NAME:  NAME,
		ROOMS: make([]*tRoom, 0, 100),
		wld:   w}
	w.Zones[VNUM] = &newZone
	return &newZone
}

func (w *tWldIndex) newExit(SRC string, DST string, DIRECT string) {
	if room, e := w.Rooms[SRC]; !e {
		return
	} else {
		if _, e := room.EXITS[DIRECT]; e {
			log.Println("dup exit ", room.VNUM, DIRECT)
			return
		}
		room.EXITS[DIRECT] = DST
	}
}
