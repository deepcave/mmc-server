package store

import (
	"database/sql"
	"log"
)

var oldDb *sql.DB
var Db *sql.DB

var stmSaveRoom *sql.Stmt
var stmSaveZone *sql.Stmt
var stmSaveExit *sql.Stmt

var stmSaveTell *sql.Stmt

func OpenOld(file string) bool {
	log.Println("open db file ", file)
	var err error
	oldDb, err = sql.Open("sqlite3", file)
	if err != nil {
		log.Print("old Data base open error!!!!", err)
		return true
	}
	log.Println("DB", oldDb)
	return false
}

func OpenDb(file string) bool {
	var err error
	if Db, err = sql.Open("sqlite3", file); err != nil {
		log.Print("clean Data base open error!!!!", err)
		return false
	}
	return true
}

func InitStms() bool {
	if stmSaveRoom != nil {
		return true
	}
	var err error
	if stmSaveRoom, err = Db.Prepare("INSERT INTO Rooms (VNUM,NAME,ZONE,TERRAIN) VALUES (?,?,?,?)"); err != nil {
		log.Println("prepare error", err)
		return false
	}

	if stmSaveExit, err = Db.Prepare("INSERT INTO RoomExits (SRC,DST,DIRECT) VALUES(?,?,?)"); err != nil {
		log.Println("prepare error", err)
		return false
	}

	if stmSaveZone, err = Db.Prepare("INSERT INTO Zones (VNUM,NAME) VALUES(?,?)"); err != nil {
		log.Println("prepare error", err)
		return false
	}

	if stmSaveTell, err = Db.Prepare("INSERT INTO Messages (Time,Type,Name,Message) VALUES(?,?,?,?)"); err != nil {
		log.Println("#### @SQLprepare error", err)
		return false
	} else {
		log.Println("### @SQLPrepare OK")
	}
	return true
}

func MsdpSaveRoom(msdp map[string]interface{}) {
	InitStms()
	VNUM, _ := msdp["VNUM"]
	NAME, _ := msdp["NAME"]
	ZONE, _ := msdp["ZONE"]
	AREA, _ := msdp["AREA"]
	TERRAIN, _ := msdp["TERRAIN"]
	var room *tRoom
	var zone *tZone
	if zone = wld.new.GetZone(ZONE.(string)); zone == nil {
		log.Println("MSDP NEW ZONE", ZONE, AREA)
		zone = wld.new.newZone(ZONE.(string), AREA.(string))
		stmSaveZone.Exec(ZONE, AREA)
	}
	if room = wld.new.GetRoom(VNUM.(string)); room == nil {
		log.Println("MSDP NEW ROOM", ZONE, VNUM, NAME, TERRAIN)
		room = wld.new.newRoom(VNUM.(string), NAME.(string), ZONE.(string), TERRAIN.(string))
		stmSaveRoom.Exec(VNUM, NAME, ZONE, TERRAIN)
	}

	EXITS, _ := msdp["EXITS"]
	for DIRECT, DST := range EXITS.(map[string]interface{}) {
		if _, e := room.EXITS[DIRECT]; e {
			continue
		}
		log.Println("MSDP NEW EXIT", VNUM, DST, DIRECT)
		room.setExit(DIRECT, DST.(string))
		stmSaveExit.Exec(VNUM, DST, DIRECT)
	}

}
