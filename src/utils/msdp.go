package utils

import (
	"log"
)

func msdpParseBuff(buffer []byte) {

}

func msdpParseVal(buffer []byte, separator byte) (interface{}, int) {
	bufferLen := len(buffer)
	coo := 0
	coa := 0
	val := make([]byte, 0, 10240)
	p := 0
	for ; p < bufferLen; p++ {
		switch buffer[p] {
		case '[':
			coo++
			break
		case ']':
			coo--
			break
		case '{':
			coa++
			break
		case '}':
			coa--
			break
		default:
			break
		}
		if buffer[p] == separator && coa == 0 && coo == 0 {
			break
		}
		val = append(val, buffer[p])
	}

	if len(val) == 0 {
		return nil, p
	}
	var rval interface{}
	switch val[0] {
	case '[':
		rval = msdpParseArray(val[1 : len(val)-1])
		break
	case '{':
		rval = msdpParseObject(val[1 : len(val)-1])
		break
	default:
		rval = string(val)
		break
	}
	return rval, p
}

func msdpParseArray(buffer []byte) interface{} {
	result := make([]interface{}, 0, 128)
	bufferLen := len(buffer)

	for p := 0; p < bufferLen; p++ {
		if buffer[p] == ':' {
			p++
			val, size := msdpParseVal(buffer[p:], ':')
			result = append(result, val)
			p += size - 1
		}
	}
	return result
}

func msdpParseObject(buffer []byte) map[string]interface{} {
	bufferLen := len(buffer)
	obj := make(map[string]interface{})
	p := 0

	for p < bufferLen {
		if buffer[p] == '#' {
			p++
			name := make([]byte, 0, 32)
			for ; buffer[p] != ':'; p++ {
				name = append(name, buffer[p])
			}
			p++
			val, size := msdpParseVal(buffer[p:], '#')
			p += size
			obj[string(name)] = val
		} else {
			p++
			log.Print("some ne tak")
		}
	}
	return obj
}

func MsdpDecode(buffer []byte) map[string]interface{} {
	result := msdpParseObject(buffer)
	return result
}
