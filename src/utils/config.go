package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

var Config map[string]string

func ReadConfig() map[string]string {
	buffer, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Print("error reading config")
	}
	err = json.Unmarshal(buffer, &Config)
	if err != nil {
		log.Println("error decode config ", string(buffer), "\n##\n", err)
	}
	return Config
}
