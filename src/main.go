package main

import (
	"log"

	"./events"
	"./mmc"
	"./store"
	"./utils"
	"./ws"
)

const LOCAL_PORT = 6011

func main() {
	log.Printf("Starting mmc server")
	cfg := utils.ReadConfig()
	store.Init(cfg["dbpath"])

	mmc.Init()
	ws.Start()

	for {
		line := mmc.GetIn()
		ev := *events.Parse(line)
		ws.Send(ev.Route())
	}

	log.Println("ERROR: end of code")
}
