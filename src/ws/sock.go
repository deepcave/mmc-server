package ws

import (
	"encoding/json"
	"log"

	"../store"
	"github.com/googollee/go-socket.io"
)

var Sock *socketio.Server

func Send(o interface{}) {
	if o == nil {
		return
	}
	Sock.BroadcastTo("mmc", "event", o)
}

func init() {
	log.Println("INIT socket.io server")
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	server.On("connection", func(so socketio.Socket) {
		log.Printf("INFO: Have a new client %s", so.Id())
		so.Emit("ept", "new")
		so.Join("mmc")

		so.On("disconnection", func() {
			for _, name := range so.Rooms() {
				log.Printf("INFO: disconnected %s [%s]", name, so.Id())
			}
		})

		so.On("mapGetZone", func(zoneNum string) {
			log.Print("INFO: get mapGetZone")
			z := store.Wld().GetZone(zoneNum)
			log.Println("arg %v", zoneNum, z)
			b, _ := json.Marshal(z)
			e1 := so.Emit("zoneData", string(b))
			log.Println("MAPI SEND", e1)
		})
	})

	server.On("error", func(so socketio.Socket, err error) {
		log.Println("ERROR: webSocket error ", err)
	})

	Sock = server
}

func getSocketIO() *socketio.Server {
	return Sock
}
