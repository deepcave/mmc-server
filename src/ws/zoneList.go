package ws

import (
	"log"
	"net/http"

	"../store"
)

func apiGetZoneList(w http.ResponseWriter, r *http.Request) {
	result := make(map[string]string)
	log.Println("GET ZONE LIST REQUEST")
	for vnum, zone := range store.Wld().Zones {
		result[vnum] = zone.NAME
	}
	replay := apiReplay{
		ErrorCode:    0,
		ErrorMessage: "",
		Result:       result}
	replay.send(w)
}

func apiGetZone(w http.ResponseWriter, r *http.Request) {
	request := newApiRequest(r)
	zoneVNUM := request.get("zone")
	if zoneVNUM == nil {
		apiError(w, 100, "Missing zone vnum")
		return
	}
	zone := store.Wld().GetZone((*zoneVNUM).(string))
	log.Println("get request for zone ", zone)
	replay := apiReplay{
		ErrorCode:    0,
		ErrorMessage: "",
		Result:       zone}
	replay.send(w)
}

func apiGetChat(w http.ResponseWriter, r *http.Request) {

	log.Println("get request for chat messages ")
	replay := apiReplay{
		ErrorCode:    0,
		ErrorMessage: "",
		Result:       store.TellGets()}
	replay.send(w)
}
