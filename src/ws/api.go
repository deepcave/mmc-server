package ws

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type apiReplay struct {
	ErrorCode    int
	ErrorMessage string
	Result       interface{}
}

type apiRequest struct {
	Token   string
	Error   int
	Request map[string]interface{}
}

func newApiRequest(r *http.Request) *apiRequest {
	rBody, err := ioutil.ReadAll(r.Body)
	params := apiRequest{Error: 0}
	if err != nil {
		log.Println("have somer error in body", err)
		params.Error = 1
		return &params
	}
	err = json.Unmarshal(rBody, &params)
	if err != nil {
		log.Println("unmarshall")
	}
	return &params
}

func apiError(w http.ResponseWriter, errorCode int, errorMessage string) {
	replay := apiReplay{
		ErrorCode:    errorCode,
		ErrorMessage: errorMessage}
	replay.send(w)
}

func (r *apiRequest) get(name string) *interface{} {
	val, e := r.Request[name]
	log.Println("get ", name, r.Request, val)
	if !e {
		return nil
	}
	return &val
}

func (r *apiReplay) send(w http.ResponseWriter) bool {
	replay, err := json.Marshal(r)
	if err != nil {
		log.Println("API Marshaling error", err)
		return false
	}
	w.Write(replay)
	return true
}
