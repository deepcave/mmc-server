package ws

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func Start() {
	sockServer := getSocketIO()
	xmux := http.NewServeMux()
	router := mux.NewRouter()

	//hack
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
	})
	//it`s no time for api
	//router.HandleFunc("/mmc/api/send/{name}/{line}", wsMudSend).Methods("GET")
	router.HandleFunc("/mmc/api/zones/list", apiGetZoneList).Methods("POST")
	router.HandleFunc("/mmc/api/zones/get", apiGetZone).Methods("POST")
	router.HandleFunc("/mmc/api/chat/get", apiGetChat).Methods("POST")
	router.PathPrefix("/mmc/ui").Handler(http.StripPrefix("/mmc/ui", http.FileServer(http.Dir("./public"))))
	xmux.Handle("/mmc/api/socket.io/", c.Handler(sockServer))
	xmux.Handle("/", c.Handler(router))

	go http.ListenAndServe(fmt.Sprintf(":%d", 6011), xmux)
}
