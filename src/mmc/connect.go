package mmc

import (
	"fmt"
	"log"
	"net"
	"strings"
	"time"
)

const BUFFER_SIZE = 1024

var mmcHost string = "localhost"
var mmcPort string = "6010"
var conn net.Conn
var chanIn chan string
var chanOut chan string

func Init() {
	chanIn = make(chan string)
	chanOut = make(chan string)
	go Connect()
}

func GetIn() string {
	return <-chanIn
}

func Connect() {
	log.Printf("Starting connection loop")
	for {
		var err error
		conn, err = net.Dial("tcp", fmt.Sprintf("%s:%s", mmcHost, mmcPort))
		if err != nil {
			log.Printf("WARN: Can`t conntct to mmc")
			time.Sleep(1 * time.Second)
			continue
		}
		b := make([]byte, BUFFER_SIZE*2)
		buffer := make([]byte, 0, BUFFER_SIZE*10)
		for {
			n, err := conn.Read(b)
			if err != nil {
				break
			}
			if n > 0 {
				if b[n-1] == 10 {
					buffer = append(buffer, b[0:n-1]...)
					for _, str := range strings.Split(string(buffer), "\n") {
						chanIn <- str
					}
					buffer = make([]byte, 0, BUFFER_SIZE*10)
				} else {
					buffer = append(buffer, b[0:n]...)
				}
			} else {
				fmt.Printf("ERROR some gone netak\n")
				time.Sleep(1 * time.Second)
				break
			}
		}

	}
}

func setPort(port string) {
	mmcPort = port
}
